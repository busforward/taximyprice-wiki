# Назначение и задачи блога

**Блог** - это мощный инструмент продвижения бизнеса.
		
Он выполняет следующие функции:
		
**Привлекает целевых клиентов из поисковых систем.**

Посредством ключевых слов, у каждой отдельной статьи свой набор, люди будут приходить из поисковх систем, чтобы получить нужную им информацию. В конце каждой статьи находятся целевые ссылки с призывом к действию для 2-х типов наших клиентов и партнеров. 

Потенциальные клиенты нажимают их и в случае оформления покупки / заключения партнерского договора становятся действующими клиентами или партнерами.

**Привлекает потенциальных клиентов из соц.сетей и блоговых систем.**

Пользователи, если статья была им полезной, могут поделиться ей в соц.сетях. Анонсы на статьи также будут публиковаться на наших страницах в соц.сетях. Наши медиа партнеры могут использовать ссылки на статьи блога (с добавлением их партнерского кода) для привлечения потенциальных клиентов.

Люди увидевшие информацию и заинтересовавшиеся переходят по ссылке на статью также могут перейти в разряд наших клиентов или партнеров, либо поделиться информацией с другими людьми.
	
**Информационная функция. Новые и повторные продажи.**

Наши клиенты, партнеры или новые пользователи могут подписаться на рассылку и получать уведомления о новых статьях или видео на почту. Переходить по ссылке на блог и получать информацию о текущих продуктах/услугах (которые возможно пользователь еще не пробовал), о новых продуктах/услугах, программах, конкурсах. Это способствует новым продажам, если ранее клиент зарегистрировался, а потом дозрел, а также повторным продажам.
	
**SEO продвижение.**

На странице каждой отдельной статьи также находится список статей из категории текущей статьи, на главной странице есть поиск и фильтры, чтобы найти нужную информацию. Переходы пользователей по страницам, задержка их на сайте способствует укреплению позиций сайта в поисковиках.

Эффективное использование ключевых слов, поиск ключевых запросов и анализ поисковой выдачи, написание статей, особенно по недостаточно удовлетворенным запросам, способствует впоследствии попаданию статей в ТОП поисковой выдачи. А это самый дешевый способ получения потенциальных клиентов.
	
**Информационная поддержка.**

Служба поддержки может использовать отдельные статьи блога, чтобы удовлетворить вопросы, возникающие у клиентов.
	
**Повышает уровень доверия к проекту.**

Клиенты уже купившие наши услуги, и получившие положительный опыт, будут относиться с большим доверием и к последующей информации, опубликованной в блоге.

Новые пользователи, увидев количество отметок Useful (полезность статьи) и положительные комментарии, также будут относиться к информации с большим доверием.
	
**Сохраняет нужную информацию.**

Пользователь может добавить важную для него статью в свою папку My Library, чтобы потом в любой момент быстро найти ее, освежить информацию или поделиться ей.
	
**Усиливает бренд.**

Блог позволяет укрепить бренд, подчеркнуть ценности, которые нам близки. Позволяет нам поделиться своим мнением и видением. Рассказать о наших благотворительных проектах. Создать сообщество. Рассказать о нашей компании, сотрудниках, чем мы живем, как проводим время. Привлечь новых специалистов.
