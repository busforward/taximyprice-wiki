Страницы сайта
==============

.. toctree::
   :maxdepth: 2

   sitemap
   template/index
   home/index
   registration/index
   offices/index
   blog/index

