.. TMP Wiki documentation master file, created by
Пользовательская документация проекта Taxi My Price (TMP)
=========================================================

.. _home-docs:

.. toctree::
   :maxdepth: 2
   :caption: Главная:

.. _tesaurus-docs:

.. toctree::
   :maxdepth: 2
   :caption: Словарь терминов:

   tesaurus/index

.. _target-docs:

.. toctree::
   :maxdepth: 2
   :caption: Цели и задачи:

   target/index

.. _terms-docs:

.. toctree::
   :maxdepth: 2
   :caption: Правила заполнения документа:

   terms/index

.. _requirements-docs:

.. toctree::
   :maxdepth: 2
   :caption: Требования к ПО:

   requirements/index

.. _design-docs:

.. toctree::
   :maxdepth: 2
   :caption: Дизайн сайта:

   design/index

.. _users-docs:

.. toctree::
   :maxdepth: 2
   :caption: Пользователи:

   users/index

.. _pages-docs:

.. toctree::
   :maxdepth: 2
   :caption: Страницы сайта:

   pages/index

.. Индексы и таблицы
.. -----------------

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
